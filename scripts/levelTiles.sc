import scala.util.Random

@main def main(size: Int) = {
  val maxFill = 10
  val decayFn = (xs: List[Int]) => {
    val avg   = Random.shuffle(xs.sorted.reverse.take(4)).head
    val decay = Random.nextGaussian().abs * 1.5
    val v     = (avg - decay).round.toInt
    if (v < 0) 0 else v
  }
  Grid.generate(size, maxFill, decayFn)
    .printColor
}

case class Grid private (size: Int, matrix: Array[Array[Int]]) {
  import Grid.Coord

  private def apply(c: Coord) =
    matrix(c.y)(c.x)

  private def set(c: Coord, v: Int) =
    matrix(c.y)(c.x) = v

  private def mapXY(fn: (Coord => Int)): Grid =
    this.copy(matrix = matrix.zipWithIndex.map { case (row, y) =>
      row.zipWithIndex.map { case (v, x) =>
        fn(Coord(x, y, size))
      }
    })

  private def withBorder(v: Int): Grid =
    mapXY {
      case c: Coord if c.isBorder => v
      case c: Coord               => this(c)
    }

  @SuppressWarnings(
    Array(
      "scalafix:DisableSyntax.var",
      "scalafix:DisableSyntax.while"
    )
  )
  private def foreachSpiral(fn: Coord => Unit) = {
    var m = size
    var n = size
    var i = 0
    var k = 0
    var l = 0

    /** k - starting row index m - ending row index l - starting column index n - ending column
      * index i - iterator
      */
    while (k < m && l < n) {
      // Print the first row from the remaining rows
      for (i <- l until n) yield fn(Coord(i, k, size))
      k = k + 1

      // Print the last column from the remaining
      // columns
      for (i <- k until m) yield fn(Coord(n - 1, i, size))
      n = n - 1

      // Print the last row from the remaining rows */
      if (k < m) {
        for (i <- (n - 1) to l by -1) yield fn(Coord(i, m - 1, size))
        m = m - 1
      }

      // Print the first column from the remaining
      // columns */
      if (l < n) {
        for (i <- (m - 1) to k by -1) yield fn(Coord(l, i, size))
        l = l + 1
      }
    }
  }

  private def decayInterior(decayFn: List[Int] => Int): Grid = {
    foreachSpiral { coord =>
      val v = coord match {
        case c: Coord if c.isBorder => this(c)
        case c: Coord               => decayFn(c.around.map(this(_)))
      }
      set(coord, v)
    }
    this
  }

  private def print = matrix.foreach { row =>
    val rowString = row.mkString(" ")
    println(s"$rowString")
  }

  private def printDirection = matrix.zipWithIndex.foreach { case (row, y) =>
    val rowString = row.zipWithIndex
      .map { case (v, x) =>
        Coord(x, y, size).direction
      }
      .mkString(" ")
    println(s"$rowString")
  }

  def printColor = {
    val max  = matrix.flatMap(row => row).max
    val high = (3.0 / 4) * max
    val mid  = (1.0 / 2) * max
    val low  = (1.0 / 4) * max
    matrix.foreach { row =>
      val rowString = row
        .map {
          case v if v == max             => "■"
          case v if v >= high && v < max => "▩"
          case v if v >= mid && v < high => "▧"
          case v if v >= low && v < mid  => "▥"
          case v if v >= 0 && v < low    => " "
        }
        .mkString(" ")
      println(s"$rowString")
    }
  }

  def tuples = matrix.zipWithIndex.flatMap { case (row, y) =>
    row.zipWithIndex
      .map { case (v, x) =>
        (x, y, v)
      }.toList
  }.toList
}

object Grid {
  def generate(size: Int, maxFill: Int, decayFn: List[Int] => Int): Grid =
    Grid(size)
      .withBorder(maxFill)
      .decayInterior(decayFn)

  private def apply(size: Int): Grid = Grid(size, Array.tabulate(size, size)((x, y) => 0))

  private case class Coord(x: Int, y: Int, size: Int) {
    require(x >= 0 && x < size, s"x = $x should be in [0, ${size - 1}]")
    require(y >= 0 && y < size, s"y = $x should be in [0, ${size - 1}]")

    private def up       = copy(y = y - 1)
    private def left     = copy(x = x - 1)
    private def down     = copy(y = y + 1)
    private def right    = copy(x = x + 1)
    def around           = List(up, up.right, right, down.right, down, down.left, left, up.left)

    private def isCorner = x == y || x + y + 1 == size
    private def isTop    = y < size / 2
    private def isBottom = y >= size / 2
    private def isLeft   = x < size / 2
    private def isRight  = x >= size / 2
    def isTopLeft        = isCorner && isTop && isLeft
    def isTopRight       = isCorner && isTop && isRight
    def isBottomLeft     = isCorner && isBottom && isLeft
    def isBottomRight    = isCorner && isBottom && isRight
    def isTopQ           = x > y && x + y + 1 < size
    def isBottomQ        = x < y && x + y >= size
    def isLeftQ          = x < y && x + y + 1 < size
    def isRightQ         = x > y && x + y >= size
    def isBorder = (x, y) match {
      case (0, _)                  => true
      case (_, 0)                  => true
      case (v, _) if v == size - 1 => true
      case (_, v) if v == size - 1 => true
      case _                       => false
    }

    def direction = this match {
      case _ if isBorder      => '■'
      case _ if isTopLeft     => '↖'
      case _ if isTopRight    => '↗'
      case _ if isBottomLeft  => '↙'
      case _ if isBottomRight => '↘'
      case _ if isTopQ        => '↑'
      case _ if isBottomQ     => '↓'
      case _ if isLeftQ       => '←'
      case _ if isRightQ      => '→'
      case _                  => '·'
    }
  }
}
