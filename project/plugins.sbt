addSbtPlugin("org.scala-js"             %% "sbt-scalajs"  % "1.9.0")
addSbtPlugin("io.indigoengine"          %% "sbt-indigo"   % "0.12.1")
addSbtPlugin("ch.epfl.scala"             % "sbt-scalafix" % "0.9.31")
