#version 300 es

precision mediump float;

vec2 UV;
float TIME;
vec4 COLOR;

//<indigo-fragment>
layout (std140) uniform RadiationTileData {
  vec2 LOCATION;
  float SIEVERT;
  float RANDOM_1;
  float RANDOM_2;
};

// -- gradient noise --
vec2 hash( in vec2 x ) {
  const vec2 k = vec2(0.3183099, 0.3678794);
  x = x * k + k.yx;
  return -1.0 + 2.0 * fract( 16.0 * k * fract(x.x * x.y * (x.x + x.y)));
}

float perlin_noise( in vec2 p ) {
  vec2 i = floor( p );
  vec2 f = fract( p );
  vec2 u = f*f*(3.0-2.0*f);
  vec2 du = 6.0*f*(1.0-f);
  vec2 ga = hash( i + vec2(0.0,0.0) );
  vec2 gb = hash( i + vec2(1.0,0.0) );
  vec2 gc = hash( i + vec2(0.0,1.0) );
  vec2 gd = hash( i + vec2(1.0,1.0) );
  
  float va = dot( ga, f - vec2(0.0,0.0) );
  float vb = dot( gb, f - vec2(1.0,0.0) );
  float vc = dot( gc, f - vec2(0.0,1.0) );
  float vd = dot( gd, f - vec2(1.0,1.0) );

  return va + u.x*(vb-va) + u.y*(vc-va) + u.x*u.y*(va-vb-vc+vd);
}
// -- /gradient noise --

float map(float value, float inMin, float inMax, float outMin, float outMax) {
  return outMin + (outMax - outMin) * (value - inMin) / (inMax - inMin);
}

vec2 map(vec2 v, float inMin, float inMax, float outMin, float outMax) {
  float x = map(v.x, inMin, inMax, outMin, outMax);
  float y = map(v.y, inMin, inMax, outMin, outMax);
  return vec2(x, y);
}

vec2 random_offset(float r, vec2 p, vec2 o) {
  vec2 offset = map(o, 0., 1., -.5 + r, .5 - r);
  return p + offset;
}

/**
 * Create a pulsating circle with radius oscillating between radius/2 and radius.
 */
float sdfPulseCircle(vec2 p, float radius, float rand) {
  float min_mult = 0.5;
  float amp = (1. - min_mult) / 2.;
  float pulse = amp * sin(2. * TIME * rand) + amp + min_mult;
  float pulse_r = radius * pulse;
  return length(p) - pulse_r;
}

void fragment() {
  vec2 uv = UV - 0.5;
  float radius = .5;

  // create perlin noise
  float octaves = 6. + 4. * RANDOM_1;
  float perlin_n = perlin_noise(vec2(octaves * UV.x, octaves * UV.y) + vec2(sin(TIME), cos(TIME)) * RANDOM_1);

  // random offset point and add noises
  vec2 p = uv + random_offset(radius, uv, vec2(RANDOM_1, RANDOM_2));
  p = p + (perlin_n * 0.4);

  // create noised pulse circle and discretize
  float d = sdfPulseCircle(p, radius, RANDOM_2);
  float d_step = step(d, 0.0);

  if (d_step < 0.000001) {
    // if zero (+ tolerance) return transparency
    COLOR = vec4(0.);
  } else {
    // otherwise draw red with sievert alpha
    float alphaize = 0.6 + 0.3 * RANDOM_2;
    vec3 c = vec3(d_step, 0., 0.);
    vec4 color = vec4(c, SIEVERT * alphaize);
    COLOR = vec4(color.rgb * color.a, color.a);
  }
}
//</indigo-fragment>
