#version 300 es

precision mediump float;

uniform sampler2D DST_CHANNEL;
vec4 SRC;
vec4 DST;
vec2 VIEWPORT_SIZE;

vec4 COLOR;

//<indigo-fragment>
layout (std140) uniform PortalBlendData {
  vec2 DISTANCE;
  float ABSORBING;
};

// https://www.shadertoy.com/view/ftKSDm
void fragment() {
  float vortex = TIME * 4.;
  vec2 uv = vec2(UV);
  
  // The position of the black hole
  vec2 pos = ((DISTANCE - (VIEWPORT_SIZE * .5)) / VIEWPORT_SIZE) + vec2(1.);

  // The distance of the pixel from the black hole
  float radius = length(uv - pos) - ABSORBING;
  
  // The angle between the pixel and the black hole
  float angle = atan(uv.y - pos.y, uv.x - pos.x) + vortex;
  
  // How much the light is bent
  float bend = 0.005 / radius;
  
  // Bend the light towards the black hole
  uv += -bend * vec2(cos(angle), sin(angle));

  // Sample the texture
  vec4 col = texture(DST_CHANNEL, uv);

  // The black part of the black hole
  // col *= smoothstep(.5, .001, bend);

  // Output to screen
  vec4 black = vec4(0., 0., 0., 1.);
  COLOR = mix(col, black, pow(ABSORBING, 4.));
}
//</indigo-fragment>
