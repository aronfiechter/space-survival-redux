#version 300 es

precision mediump float;

vec2 POSITION;

//<indigo-vertex>
layout (std140) uniform RadiationTileData {
  vec2 LOCATION;
  float SIEVERT;
  float RANDOM_1;
  float RANDOM_2;
};

void vertex(){
  POSITION = LOCATION;
}
//</indigo-vertex>
