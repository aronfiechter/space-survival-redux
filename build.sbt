ThisBuild / version := "0.1.0"

ThisBuild / scalaVersion := "3.1.1"

ThisBuild / scalafixDependencies += "com.github.liancheng" %% "organize-imports" % "0.5.0"

lazy val root = (project in file("."))
  .enablePlugins(ScalaJSPlugin, SbtIndigo)
  .settings(
    name := "space-survival-redux",
    libraryDependencies ++= Seq(
      "org.scalameta"   %%% "munit" % "0.7.29" % Test,
      "io.indigoengine" %%% "indigo" % "0.13.0",
      "io.indigoengine" %%% "indigo-extras" % "0.13.0",
      "io.indigoengine" %%% "indigo-json-circe" % "0.13.0",
    ),
    testFrameworks += new TestFramework("munit.Framework"),
    Test / scalaJSLinkerConfig ~= { _.withModuleKind(ModuleKind.CommonJSModule) },
    scalafixOnCompile := true,
    semanticdbEnabled := true,
    semanticdbVersion := scalafixSemanticdb.revision
  )
  .settings( // Indigo specific
    title := "Space Survival Redux",
    showCursor := true,
    disableFrameRateLimit := true,
    gameAssetsDirectory := "assets",
    windowStartWidth := 720, // Needed for Electron
    windowStartHeight := 720, // Needed for Electron
  )

addCommandAlias("buildGame", ";compile;fastOptJS;indigoBuild")
addCommandAlias("runGame", ";compile;fastOptJS;indigoRun")
addCommandAlias("buildGameFull", ";compile;fullOptJS;indigoBuildFull")
addCommandAlias("runGameFull", ";compile;fullOptJS;indigoRunFull")
