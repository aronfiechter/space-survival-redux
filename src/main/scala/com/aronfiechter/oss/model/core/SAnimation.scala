package com.aronfiechter.oss.model.core

import indigo._

type ClipsMap = Map[CycleLabel, Clip[Material.Bitmap]]

final case class SAnimation(val label: CycleLabel, val startTime: Seconds) {
  def playOnce(using clipsMap: ClipsMap) = clipsMap(label).playOnce(startTime)
  def loop(using clipsMap: ClipsMap)     = clipsMap(label)
  def duration(using clipsMap: ClipsMap) =
    clipsMap(label).sheet.frameDuration * clipsMap(label).sheet.frameCount

  def isFinished(currentTime: Seconds)(using clipsMap: ClipsMap) =
    startTime + duration < currentTime
}
