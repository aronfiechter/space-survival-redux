package com.aronfiechter.oss.model.level.spaceship

import com.aronfiechter.oss.events._
import com.aronfiechter.oss.model.core._
import com.aronfiechter.oss.model.level.space._
import com.aronfiechter.oss.utils._
import indigo._
import indigoextras.geometry.BoundingBox

final case class Spaceship(
    position: Vector2,
    speed: Vector2,
    shield: Shield,
    energy: EnergyCell,
    engine: Engine,
) {

  private val maxSpeed = 3000

  def withSpeed(newSpeed: Vector2) =
    this.copy(speed = newSpeed.clampToMagnitude(maxSpeed))

  def hit(damage: Int): Outcome[Spaceship] =
    this.shield.hit(damage).map { newShield =>
      this.copy(shield = newShield)
    }

  private def withPosition(newPosition: Vector2) =
    this.copy(position = newPosition)

  private def boundingBox = BoundingBox(32, 32).moveTo(position).moveBy(-16, -16)

  def moveTowards(direction: Vector2): Outcome[Spaceship] =
    this.energy.use(this.engine.boostEnergyCost)
      .map { newEnergy => this.copy(energy = newEnergy) }
      .mapGlobalEvents {
        case EnergyUsedEvent => {
          val boostForce = this.engine.boostForce
          val pushVector = (direction - position).normalise * boostForce
          val newSpeed   = speed + pushVector
          ChangeSpeedEvent(newSpeed)
        }
        case a => a
      }

  def update(gameTime: GameTime, space: Space): Outcome[Spaceship] = {
    val Δt = gameTime.delta.toDouble
    val scaledSpeed = speed scaleBy Δt
    val newPosition = position + scaledSpeed
    val newSpaceship = this
      .withPosition(newPosition)
      .copy(energy = energy.recharge(Δt))
    val optDamage = space.tileAt(newPosition).map(_.fill.radDamage)
    optDamage match {
      case None => Outcome(newSpaceship)
      case Some(damage) => Outcome(newSpaceship)
        .addGlobalEvents(DamageEvent(damage))
    }
  }
}

case class Shield(current: Int, max: Int, damagePercent: Float = 1.0) {
  lazy val percent: Float = current.toFloat / max
  
  def hit(damage: Int): Outcome[Shield] =
    (current - damage * damagePercent) match {
      case newCurrent if newCurrent <= 0 =>
        Outcome(this.copy(current = 0))
          .addGlobalEvents(DestroyedEvent)
      case newCurrent => Outcome(this.copy(newCurrent.toInt))
    }
}

object BasicShield extends Shield(10000, 10000)
object InfiniteShield extends Shield(10000, 10000, 0.0)

case class EnergyCell(current: Int, max: Int, rechargeRate: Int) {
  lazy val percent: Float = current.toFloat / max

  def use(amount: Int): Outcome[EnergyCell] =
    (current - amount) match {
      case newCurrent if newCurrent < 0 =>
        Outcome(this).addGlobalEvents(NotEnoughEnergyEvent)
      case newCurrent =>
        Outcome(this.copy(current = newCurrent)).addGlobalEvents(EnergyUsedEvent)
    }

  def recharge(Δt: Double): EnergyCell =
    this.copy(current = Math.min(max, current + (Δt * rechargeRate).toInt))
}

object BasicEnergyCell extends EnergyCell(10000, 10000, 200)
object InfiniteEnergyCell extends EnergyCell(10000, 10000, 10000)

case class Engine(boostEnergyCost: Int, boostForce: Int)
object BasicEngine extends Engine(500, 150)
