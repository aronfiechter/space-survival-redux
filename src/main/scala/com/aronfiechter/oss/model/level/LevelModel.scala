package com.aronfiechter.oss.model.level

import com.aronfiechter.oss.boot._
import com.aronfiechter.oss.constants.Constants.Portal.MIN_DISTANCE
import com.aronfiechter.oss.model.core._
import com.aronfiechter.oss.model.level.portal._
import com.aronfiechter.oss.model.level.space._
import com.aronfiechter.oss.model.level.spaceship._
import com.aronfiechter.oss.utils.Level
import indigo._

import scala.util.Random

final case class LevelInfo(number: Int) {
  lazy val size = Level.size(number)
  def inc = this.copy(number = number + 1)
}
object LevelInfo {
  def first = LevelInfo(1)
}
final case class LevelModel(
    levelInfo: LevelInfo,
    spaceship: Spaceship,
    space: Space,
    portal: Portal,
) {

  def nextLevel(dice: Dice, startupData: StartupData) = {
    // generate
    val newModel = LevelModel.generate(dice, startupData, levelInfo = levelInfo.inc)
    newModel
      // copy spaceship but change position
      .copy(spaceship = spaceship.copy(position = newModel.spaceship.position))
  }

  def restart(dice: Dice, startupData: StartupData) = {
    // generate initial model
    LevelModel.initial(dice, startupData)
  }

  def moveTowards(position: Vector2): Outcome[LevelModel] =
    spaceship.moveTowards(position).map { newSpaceship =>
      this.copy(spaceship = newSpaceship)
    }

  def update(gameTime: GameTime): Outcome[LevelModel] =
    spaceship.update(gameTime, space)
      .flatMap { s =>
        portal.update(s, gameTime).combine(Outcome(s))
      }.map { case (p, s) =>
        this.copy(spaceship = s, portal = p)  
      }
}

object LevelModel {
  def initial(dice: Dice, startupData: StartupData): LevelModel =
    LevelModel.generate(dice, startupData: StartupData, LevelInfo.first)

  def generate(dice: Dice, startupData: StartupData, levelInfo: LevelInfo): LevelModel = {
    val space = Space(gridSize = levelInfo.size, dice, startupData)
    val spaceship = Spaceship(
      space.center.toVector,
      Vector2.zero,
      BasicShield,
      BasicEnergyCell,
      // InfiniteShield,
      // InfiniteEnergyCell,
      BasicEngine,
    )
    val portal = getPortal(space, spaceship, dice)
    LevelModel(
      levelInfo,
      spaceship,
      space,
      portal,
    )
  }

  private def getPortal(space: Space, spaceship: Spaceship, dice: Dice): Portal = {
    val validTiles = space.tiles
      .filter(_.isEmpty)
      .filter { _.position.distanceTo(spaceship.position.toPoint) > MIN_DISTANCE }
    val idx = Random(dice.seed).nextInt(validTiles.length)
    Portal(validTiles(idx).position)
  }
}
