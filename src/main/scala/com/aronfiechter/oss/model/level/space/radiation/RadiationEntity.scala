package com.aronfiechter.oss.model.level.space.radiation

import com.aronfiechter.oss.constants.Constants.Space.TILE_SIZE
import com.aronfiechter.oss.model.level.space.Space.TileFill
import indigo._

final case class RadiationEntity(
  fill: TileFill,
  position: Point,
  depth: Depth = Depth.zero,
) extends EntityNode[RadiationEntity] with Cloneable {

  def size: Size        = TILE_SIZE
  def flip: Flip        = Flip.default
  def ref: Point        = size.toPoint
  def rotation: Radians = Radians.zero
  def scale: Vector2    = Vector2(4)

  def withDepth(newDepth: Depth): RadiationEntity =
    this.copy(depth = newDepth)

  def eventHandler: ((RadiationEntity, GlobalEvent)) => Option[GlobalEvent] =
    Function.const(None)
  def eventHandlerEnabled: Boolean = false

  def toShaderData: ShaderData =
    ShaderData(
      RadiationEntity.shaderId,
      UniformBlock(
        "RadiationTileData",
        Batch(
          Uniform("LOCATION") -> vec2(position.toVector.x, position.toVector.y),
          Uniform("SIEVERT")  -> float(fill.toAlpha),
          Uniform("RANDOM_1")   -> float(0L),
          Uniform("RANDOM_2")   -> float(0L),
        )
      )
    )

}

object RadiationEntity {

  val cloneId: CloneId       = CloneId("radiation-tile")
  val cloneBlank: CloneBlank = CloneBlank(cloneId, RadiationEntity(TileFill.None, Point.zero))

  val shaderId: ShaderId     = ShaderId("radiation tile shader")

  def shader(vertProgram: AssetName, fragProgram: AssetName): EntityShader =
    EntityShader
      .External(shaderId)
      .withVertexProgram(vertProgram)
      .withFragmentProgram(fragProgram)

}
