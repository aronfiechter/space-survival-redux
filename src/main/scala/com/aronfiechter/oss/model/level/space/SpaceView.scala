package com.aronfiechter.oss.model.level.space

import com.aronfiechter.oss.boot._
import com.aronfiechter.oss.constants.Constants.Space.TILE_SIZE
import com.aronfiechter.oss.model.level._
import com.aronfiechter.oss.model.level.space.Space.TileFill
import com.aronfiechter.oss.model.level.space.radiation.RadiationEntity
import indigo._
import indigo.shared.datatypes.Fill.Color
import indigo.syntax._

final case class SpaceView(background: Layer)

object SpaceView {
  def draw(spaceView: SpaceView): Layer = spaceView.background

  def generate(space: Space): SpaceView = {
    val dice        = Dice.fromSeed(0L)
    val size        = space.size
    val spaceBounds = Shape.Box(Rectangle(size), Fill.None, Stroke(1, RGBA.Yellow))
    val layer = Layer(
      BindingKey("space"),
      drawSpaceBackground(space.tiles),
      drawRadiation(space.tiles, dice),
      spaceBounds
    )
    val blending = Blending.Normal.withEntityBlend(Blend.Max(BlendFactor.SrcAlpha, BlendFactor.DstAlpha))
    val blendedLayer = layer.withBlending(blending)
    SpaceView(blendedLayer)
  }

  private def drawSpaceBackground(allTiles: List[Space.Tile]): Group =
    Group(
      allTiles
        .groupBy(_.id)
        .map { case (cloneId, tiles) =>
          CloneBatch(
            cloneId,
            tiles.map { tile =>
              val (x, y) = Tuple.fromProductTyped(tile.position)
              CloneBatchData(x, y, tile.rotation)
            }.toBatch
          )
        }
        .toList
        .toBatch
    )

  private def drawRadiation(allTiles: List[Space.Tile], dice: Dice): Mutants =
    Mutants(
      RadiationEntity.cloneId,
      allTiles
        .filterNot(_.isEmpty)
        .map { tile =>
          val position = tile.position + TILE_SIZE.toPoint * 2
          val (x, y) = Tuple.fromProductTyped(position.toVector)
          Batch(
            UniformBlock(
              "RadiationTileData",
              Batch(
                Uniform("LOCATION") -> vec2(x, y),
                Uniform("SIEVERT")  -> float(tile.fill.toAlpha),
                Uniform("RANDOM_1")   -> float(dice.rollFloat),
                Uniform("RANDOM_2")   -> float(dice.rollFloat),
              )
            )
          )
        }
        .toArray
    )

  /**
   * Utility function for debugging.
   */
  def drawTiles(tiles: List[Space.Tile]): List[Shape.Box] =
    tiles
      .filterNot(_.isEmpty)
      .map { tile =>
        val rect = Rectangle(TILE_SIZE)
        val v = tile.fill.toAlpha
        Shape.Box(rect, Fill.None, Stroke(2, RGBA(v, v, v)))
          .moveBy(tile.position)
          .moveBy(TILE_SIZE.toPoint / -2)
      }
}
