package com.aronfiechter.oss.model.level.portal

import com.aronfiechter.oss.model.level.spaceship.Spaceship
import indigo._

final case class PortalBlend(
  portal: Portal,
  spaceship: Spaceship,
) extends BlendMaterial derives CanEqual {
  lazy val distance = portal.position.toVector - spaceship.position;

  def toShaderData: BlendShaderData =
    BlendShaderData(
      PortalBlend.shaderId,
      UniformBlock(
        "PortalBlendData",
        Batch(
          Uniform("DISTANCE") -> vec2(distance.x, distance.y),
          Uniform("ABSORBING") -> float(portal.absorbingPercent)
        )
      )
    )

}

object PortalBlend {
  val shaderId: ShaderId     = ShaderId("portal blend shader")

  def shader(fragProgram: AssetName) =
    BlendShader
      .External(shaderId)
      .withFragmentProgram(fragProgram)
}