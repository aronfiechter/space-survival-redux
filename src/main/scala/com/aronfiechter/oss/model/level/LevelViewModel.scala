package com.aronfiechter.oss.model.level

import com.aronfiechter.oss.boot._
import com.aronfiechter.oss.model.core._
import com.aronfiechter.oss.model.level.space.Space.TileFill
import com.aronfiechter.oss.model.level.space._
import indigo._

final case class LevelViewModel(
  hudView: HudView,
  spaceView: SpaceView,
  spaceshipView: SpaceshipView
) {
  def nextLevel(startUpData: StartupData, model: LevelModel) =
    this.copy(spaceView = SpaceView.generate(model.space))

  def update(gameTime: GameTime) =
    this.copy(spaceshipView = spaceshipView.update(gameTime))
    
  def boostAnimation(gameTime: GameTime) =
    val newSpaceshipView = spaceshipView.withAnimation("Accelerate", gameTime.running)
    this.copy(spaceshipView = newSpaceshipView)
}

object LevelViewModel {
  def initial(startupData: StartupData, levelModel: LevelModel): LevelViewModel = {
    val sprites = startupData.sprites
    val clips = startupData.clips
    LevelViewModel(
      HudView(),
      SpaceView.generate(levelModel.space),
      SpaceshipView(sprites.spaceship, clips.spaceship)
    )
  }
}

final case class HudView()

final case class SpaceshipView(
  sprite: Sprite[Material.Bitmap],
  clips: ClipsMap,
  animation: Option[SAnimation] = None
) {
  // Make clips available
  given clipsGiven: ClipsMap = clips

  def withAnimation(label: String, time: Seconds) =
    this.copy(animation = Some(SAnimation(CycleLabel(label), time)))
  def withNoAnimation = this.copy(animation = None)

  def update(gameTime: GameTime) = animation match {
    case Some(animation) if (animation.isFinished(gameTime.running)) =>
      this.withNoAnimation
    case _ => this
  }

  def imageAtTime(gameTime: GameTime) = animation match {
    case None => sprite
    case Some(animation) => animation.playOnce
  }
}
