package com.aronfiechter.oss.model.level.space

import com.aronfiechter.oss.boot.StartupData
import com.aronfiechter.oss.constants.Constants.Space._
import com.aronfiechter.oss.model.level.space.generator._
import indigo._

import scala.util.Random


// TODO: make this constructor private for the space generator somehow
final case class Space(tiles: List[Space.Tile], size: Size) {
  lazy val center: Point = size.toPoint / 2

  def tileAt(position: Vector2): Option[Space.Tile] = {
    val coord = position.toPoint / TILE_SIZE.toPoint
    tiles.find { tile => tile.gridCoord == coord }
  }
}

object Space {

  def apply(gridSize: Int, dice: Dice, startupData: StartupData): Space =
    SpaceGenerator(gridSize, dice, startupData)

  final case class Tile(
    private val x: Int,
    private val y: Int,
    fill: TileFill,
    id: CloneId,
    rotation: Radians,
  ) {
    lazy val position = Point(x, y) * TILE_SIZE.toPoint + TILE_SIZE.toPoint / 2
    lazy val isEmpty = fill == TileFill.None
    lazy val gridCoord = Point(x, y)
  }

  enum TileFill(val radDamage: Int) {
    case None extends TileFill(0)
    case Low extends TileFill(10)
    case Mid extends TileFill(100)
    case High extends TileFill(1000)
    case Max extends TileFill(10000)

    def toAlpha = ordinal.toFloat / 4.0f // number of possible values - 1
  }
}
