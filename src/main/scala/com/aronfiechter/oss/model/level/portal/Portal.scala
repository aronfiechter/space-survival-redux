package com.aronfiechter.oss.model.level.portal

import com.aronfiechter.oss.events.AbsorbedEvent
import com.aronfiechter.oss.model.level.spaceship.Spaceship
import indigo._

final case class Portal(position: Point, absorbing: Int,  absorbed: Boolean) {
  import Portal._
  def update(spaceship: Spaceship, gameTime: GameTime): Outcome[Portal] = {
    val Δt = gameTime.delta.toDouble
    absorbed match {
      case true => Outcome(this)
      case false =>
        distanceTo(spaceship) match {
          case d if inRange(d) && absorbed(d, Δt) =>
            Outcome(incAbsorbing(d, Δt).absorb)
              .addGlobalEvents(AbsorbedEvent)
          case d if d < RADIUS => Outcome(incAbsorbing(d, Δt))
          case d => Outcome(decAbsorbing(d, Δt))
        }
    }
  }

  def absorbingPercent = absorbing.toFloat / ABSORBED

  private def absorb = this.copy(absorbed = true)
  
  private def delta(d: Int, Δt: Double): Int =
    (ABSORBIING_SPEED * Δt * (RADIUS - d).toFloat).toInt

  private def inRange(d: Int): Boolean = d < RADIUS
  private def increasedAbsorbing(d: Int, Δt: Double): Int = absorbing + delta(d, Δt)
  private def absorbed(d: Int, Δt: Double): Boolean = increasedAbsorbing(d, Δt) >= ABSORBED

  private def incAbsorbing(d: Int, Δt: Double) = this.copy(absorbing = increasedAbsorbing(d, Δt))
  private def decAbsorbing(d: Int, Δt: Double) = this.copy(absorbing = Math.max(absorbing + delta(d, Δt), 0))

  private def distanceTo(spaceship: Spaceship): Int = 
    spaceship.position.toPoint.distanceTo(this.position).toInt
}

object Portal {
  private val ABSORBIING_SPEED = 8.0
  private val ABSORBED = 1024
  private val RADIUS = 128

  def apply(position: Point): Portal = Portal(position, 0, false)
}