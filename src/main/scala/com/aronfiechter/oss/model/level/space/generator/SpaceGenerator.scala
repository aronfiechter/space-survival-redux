package com.aronfiechter.oss.model.level.space.generator

import com.aronfiechter.oss.boot.StartupData
import com.aronfiechter.oss.constants.Constants.Space._
import com.aronfiechter.oss.model.level.space._
import indigo._

import scala.util.Random

object SpaceGenerator {
  def apply(innerGridSize: Int, dice: Dice, startupData: StartupData): Space = {
    val cloneIds = startupData.cloneBlanks.space.map(_.id)
    val tiles = generateTiles(innerGridSize, dice, cloneIds.toList)
    val size  = TILE_SIZE * innerGridSize
    Space(tiles, size)
  }

  private def generateTiles(innerGridSize: Int, dice: Dice, ids: List[CloneId]): List[Space.Tile] = {
    val rng  = Random(dice.seed)
    val decayFn = (xs: List[Int]) => {
      val avg   = rng.shuffle(xs.sorted.reverse.take(4)).head
      val decay = rng.nextGaussian().abs * 1.5
      val v     = (avg - decay).round.toInt
      if (v < 0) 0 else v
    }
    val maxFill = 10
    val border  = 6
    val grid = Grid
      .generate(innerGridSize, maxFill, decayFn)
      .addBorder(maxFill, thickness = border)

    val gridSize = grid.size
    val tuples   = grid.tuples
    val max      = tuples.map(t => t._3).max
    val high     = (3.0 / 4) * max
    val mid      = (1.0 / 2) * max
    val low      = (1.0 / 4) * max
    val none     = 0
    
    val randomizedIds = (Stream continually rng.shuffle(ids)).flatten
    val randomizedRots = (Stream continually dice.roll(4))
    val randomPart = randomizedIds.zip(randomizedRots)
    tuples.zip(randomPart)
      .map { case ((x, y, value), (id, rot))  =>
        val fill = value match {
          case v if v == max             => Space.TileFill.Max
          case v if v >= high && v < max => Space.TileFill.High
          case v if v >= mid && v < high => Space.TileFill.Mid
          case v if v >= low && v < mid  => Space.TileFill.Low
          case v if v >= 0 && v < low    => Space.TileFill.None
        }
        val realX = x - border
        val realY = y - border
        Space.Tile(realX, realY, fill, id, Radians.PIby2 * rot)
      }
  }
}
