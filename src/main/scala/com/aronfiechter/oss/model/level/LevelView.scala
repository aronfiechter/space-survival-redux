package com.aronfiechter.oss.model.level

import com.aronfiechter.oss.boot._
import com.aronfiechter.oss.model.core._
import com.aronfiechter.oss.model.level.portal._
import com.aronfiechter.oss.model.level.space.Space.Tile
import com.aronfiechter.oss.model.level.space.Space.TileFill
import com.aronfiechter.oss.model.level.space._
import com.aronfiechter.oss.model.level.spaceship._
import com.aronfiechter.oss.utils._
import indigo._
import indigo.syntax._

object LevelView {

  def present(
      context: FrameContext[StartupData],
      model: LevelModel,
      viewModel: LevelViewModel
  ): SceneUpdateFragment = {
    val camera    = Camera.LookAt(model.spaceship.position.toPoint)
    val hudLayer  = LevelView.drawHud(model, context)
    val space     = SpaceView.draw(viewModel.spaceView)
    val spaceship = LevelView.drawSpaceship(model.spaceship, viewModel.spaceshipView, context)

    val spaceSpaceshipLayer = (space |+| Layer(spaceship)).withCamera(camera)
    val portalDistortLayer  = Layer.empty.withCamera(camera)
      .withBlendMaterial(PortalBlend(model.portal, model.spaceship))

    SceneUpdateFragment.empty
      .addLayer(spaceSpaceshipLayer)
      .addLayer(portalDistortLayer)
      .addLayer(hudLayer.withCamera(Camera.default))
  }

  private def drawHud(
      model: LevelModel,
      context: FrameContext[StartupData]
  ): Layer = Layer(
    drawLevelInfo(model.levelInfo, model.portal.absorbing, context),
    drawSpaceshipStats(model.spaceship, context)
  )

  private def drawLevelInfo(levelInfo: LevelInfo, absorbing: Int, context: FrameContext[StartupData]): TextBox = {
    val levelNumber = levelInfo.number
    val levelSize = levelInfo.size
    val fontSize = 16
    val padding  = 10
    val position = context.startUpData.viewConfig.bottomLeft
      - Point(0, fontSize)
      + Point(padding, -padding)
    val textBox = TextBox(s"Level $levelNumber (size: $levelSize, absorbing: $absorbing)")
      .withFontSize(Pixels(fontSize))
      .withFontFamily(FontFamily.sansSerif)
      .withColor(RGBA.White)
    textBox
      .withSize(Size(textBox.size.width, fontSize))
      .withPosition(position)
      .alignLeft
  }

  private def drawSpaceshipStats(spaceship: Spaceship, context: FrameContext[StartupData]): Group = {
    val fullWidth = context.startUpData.viewConfig.viewportWidth
    val barHeight = 8
    val shieldPosition = context.startUpData.viewConfig.topLeft
    val shieldWidth = (spaceship.shield.percent * fullWidth).toInt
    val energyPosition = context.startUpData.viewConfig.topLeft + Point(0, barHeight)
    val energyWidth = (spaceship.energy.percent * fullWidth).toInt

    val shieldBar = Shape.Box(Rectangle(shieldWidth, barHeight), Fill.Color(RGBA.Cyan))
      .moveTo(shieldPosition)
    val energyBar = Shape.Box(Rectangle(energyWidth, barHeight), Fill.Color(RGBA.Yellow))
      .moveTo(energyPosition)

    Group(shieldBar, energyBar)
  }

  private def drawSpaceship(
      spaceship: Spaceship,
      spaceshipView: SpaceshipView,
      context: FrameContext[StartupData]
  ): SceneNode = {
    val position         = spaceship.position.toPoint
    val viewConfig       = context.startUpData.viewConfig
    val directionToMouse = viewConfig.center.toVector - context.mouse.position.toVector
    val rotation         = directionToMouse.angle - Radians.PIby2
    spaceshipView
      .imageAtTime(context.gameTime)
      .withRef(16, 16)
      .moveTo(position)
      .withRotation(rotation)
  }
}
