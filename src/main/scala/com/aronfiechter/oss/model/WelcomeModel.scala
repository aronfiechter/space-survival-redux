package com.aronfiechter.oss.model

final case class WelcomeModel()

object WelcomeModel {
  def initial = WelcomeModel()
}
