package com.aronfiechter.oss.model

final case class EndModel()

object EndModel {
  def initial = EndModel()
}
