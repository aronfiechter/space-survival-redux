package com.aronfiechter.oss.model

import com.aronfiechter.oss.boot._
import com.aronfiechter.oss.model.level._

final case class ViewModel(level: LevelViewModel)

object ViewModel {
  def initial(startupData: StartupData, model: Model): ViewModel =
    ViewModel(LevelViewModel.initial(startupData, model.level))
}
