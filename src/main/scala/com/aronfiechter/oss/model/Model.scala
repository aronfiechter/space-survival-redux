package com.aronfiechter.oss.model

import com.aronfiechter.oss.boot._
import com.aronfiechter.oss.model.level._
import indigo._

final case class Model(
  welcome: WelcomeModel,
  level: LevelModel,
  fin: EndModel
)

object Model {
  def initial(dice: Dice, startupData: StartupData): Model =
    Model(
      welcome = WelcomeModel.initial,
      level = LevelModel.initial(dice, startupData: StartupData),
      fin = EndModel.initial,
    )
}
