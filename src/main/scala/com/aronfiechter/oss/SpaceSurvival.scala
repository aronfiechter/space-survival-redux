package com.aronfiechter.oss

import com.aronfiechter.oss.boot._
import com.aronfiechter.oss.model._
import com.aronfiechter.oss.model.level.portal._
import com.aronfiechter.oss.model.level.space.radiation.RadiationEntity
import com.aronfiechter.oss.scenes._
import indigo._
import indigo.scenes._
import indigoextras.subsystems.FPSCounter

import scala.scalajs.js.annotation.JSExportTopLevel

@JSExportTopLevel("IndigoGame")
object SpaceSurvival extends IndigoGame[ViewConfig, StartupData, Model, ViewModel] {

  def initialScene(bootData: ViewConfig): Option[SceneName] =
    Option(WelcomeScene.name)

  def scenes(bootData: ViewConfig): NonEmptyList[Scene[StartupData, Model, ViewModel]] =
    NonEmptyList(WelcomeScene, LevelScene, EndScene)

  val eventFilters: EventFilters = EventFilters.Permissive

  def boot(flags: Map[String, String]): Outcome[BootResult[ViewConfig]] =
    Outcome {
      val viewConfig: ViewConfig =
        ViewConfig.default

      val config: GameConfig =
        GameConfig.default
          .withViewport(viewConfig.viewPort)
          .withClearColor(RGBA.Indigo)
          .withMagnification(viewConfig.zoomLevel)

      BootResult(config, viewConfig)
        .withAssets(Assets.assets)
        .withFonts(Assets.fonts)
        .withShaders(
          RadiationEntity.shader(
            Assets.Space.radiationVertShader,
            Assets.Space.radiationFragShader
          ),
          PortalBlend.shader(Assets.Portal.portalBlendFragShader)
        )
        .withSubSystems(
          FPSCounter(Point(10, 26), BindingKey("fps"))
        )
    }

  def setup(
      viewConfig: ViewConfig,
      assetCollection: AssetCollection,
      dice: Dice
  ): Outcome[Startup[StartupData]] =
    StartupData.setup(viewConfig, assetCollection, dice)

  def initialModel(startupData: StartupData): Outcome[Model] =
    Outcome(Model.initial(Dice.fromSeed(System.nanoTime), startupData))

  def initialViewModel(startupData: StartupData, model: Model): Outcome[ViewModel] =
    Outcome(ViewModel.initial(startupData, model))

  def updateModel(context: FrameContext[StartupData], model: Model): GlobalEvent => Outcome[Model] =
    _ => Outcome(model)

  def updateViewModel(
      context: FrameContext[StartupData],
      model: Model,
      viewModel: ViewModel
  ): GlobalEvent => Outcome[ViewModel] =
    _ => Outcome(viewModel)

  def present(
      context: FrameContext[StartupData],
      model: Model,
      viewModel: ViewModel
  ): Outcome[SceneUpdateFragment] =
    Outcome(
      SceneUpdateFragment.empty
        .addLayer(
          Layer(BindingKey("fps"), 1, Depth.far)
            .withCamera(Camera.default)
        )
    )

}
