package com.aronfiechter.oss.events

import indigo._

trait GameEvent extends SpaceSurvivalEvent

case object NextLevelEvent extends GameEvent
case object EndGameEvent extends GameEvent

trait SpaceshipEvent extends GameEvent

// damage
case object DestroyedEvent extends SpaceshipEvent
final case class DamageEvent(amount: Int) extends SpaceshipEvent

// energy
case object NotEnoughEnergyEvent extends SpaceshipEvent
case object EnergyUsedEvent extends SpaceshipEvent

// movement
final case class ChangeSpeedEvent(newSpeed: Vector2) extends SpaceshipEvent
case object AbsorbedEvent extends SpaceshipEvent
