package com.aronfiechter.oss.scenes

import com.aronfiechter.oss.boot._
import com.aronfiechter.oss.events.AbsorbedEvent
import com.aronfiechter.oss.events.ChangeSpeedEvent
import com.aronfiechter.oss.events.DamageEvent
import com.aronfiechter.oss.events.DestroyedEvent
import com.aronfiechter.oss.events.EndGameEvent
import com.aronfiechter.oss.events.NextLevelEvent
import com.aronfiechter.oss.model._
import com.aronfiechter.oss.model.level._
import com.aronfiechter.oss.model.level.space.SpaceView
import com.aronfiechter.oss.model.level.space.radiation.RadiationEntity
import indigo._
import indigo.scenes._

object LevelScene extends Scene[StartupData, Model, ViewModel] {
  type SceneModel     = LevelModel
  type SceneViewModel = LevelViewModel

  def name: SceneName = SceneName("level")
  def modelLens: Lens[Model, LevelModel] =
    Lens(_.level, (m, level) => m.copy(level = level))
  def viewModelLens: Lens[ViewModel, LevelViewModel] =
    Lens(_.level, (m, level) => m.copy(level = level))

  def eventFilters: EventFilters = EventFilters.Permissive
  def subSystems: Set[SubSystem] = Set()

  def updateModel(
      context: FrameContext[StartupData],
      model: LevelModel
  ): GlobalEvent => Outcome[LevelModel] = {
    // user input
    case KeyboardEvent.KeyUp(Key.KEY_N) => Outcome(model).addGlobalEvents(NextLevelEvent)
    case MouseEvent.Click(clickPosition) => {
      val adjustedClickPosition =
        clickPosition.toVector - context.startUpData.viewConfig.center.toVector + model.spaceship.position
      model.moveTowards(adjustedClickPosition)
    }

    // spaceship
    case ChangeSpeedEvent(newSpeed) => Outcome {
      model.copy(spaceship = model.spaceship.withSpeed(newSpeed))
    }
    case AbsorbedEvent => Outcome(model).addGlobalEvents(NextLevelEvent)
    case DamageEvent(damage) =>
      model.spaceship.hit(damage).map { newSpaceship =>
        model.copy(spaceship = newSpaceship)
      }
    case DestroyedEvent => Outcome(model)
      .addGlobalEvents(EndGameEvent)

    // game system
    case NextLevelEvent => Outcome(model.nextLevel(context.dice, context.startUpData))
    case EndGameEvent => Outcome(model.restart(context.dice, context.startUpData))
      .addGlobalEvents(SceneEvent.JumpTo(EndScene.name))

    // frame
    case FrameTick => model.update(context.gameTime)
    case _         => Outcome(model)
  }

  def updateViewModel(
      context: FrameContext[StartupData],
      model: LevelModel,
      viewModel: LevelViewModel
  ): GlobalEvent => Outcome[LevelViewModel] =
    case NextLevelEvent => Outcome(viewModel.nextLevel(context.startUpData, model))
    case ChangeSpeedEvent(_) => Outcome(viewModel.boostAnimation(context.gameTime))
    case FrameTick           => Outcome(viewModel.update(context.gameTime))
    case _                   => Outcome(viewModel)

  def present(
      context: FrameContext[StartupData],
      model: LevelModel,
      viewModel: LevelViewModel
  ): Outcome[SceneUpdateFragment] =
    Outcome {
      LevelView
        .present(context, model, viewModel)
        .addCloneBlanks(context.startUpData.cloneBlanks.space)
        .addCloneBlanks(RadiationEntity.cloneBlank)
    }
}
