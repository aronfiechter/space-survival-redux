package com.aronfiechter.oss.scenes

import com.aronfiechter.oss.boot._
import com.aronfiechter.oss.model._
import indigo._
import indigo.scenes._

object WelcomeScene extends Scene[StartupData, Model, ViewModel] {
  type SceneModel     = WelcomeModel
  type SceneViewModel = Unit

  def name: SceneName                 = SceneName("welcome")

  def modelLens: Lens[Model, WelcomeModel] =
    Lens(_.welcome, (m, welcome) => m.copy(welcome = welcome))
  def viewModelLens: Lens[ViewModel, Unit] = Lens.unit

  val eventFilters: EventFilters =
    EventFilters.Restricted
      .withViewModelFilter(_ => None)
      
  def subSystems: Set[SubSystem]      = Set()

  def updateModel(
      context: FrameContext[StartupData],
      model: WelcomeModel
  ): GlobalEvent => Outcome[WelcomeModel] =
    case MouseEvent.Click(_) =>
      Outcome(model)
        .addGlobalEvents(SceneEvent.JumpTo(LevelScene.name))
    case _ => Outcome(model)

  def updateViewModel(
      context: FrameContext[StartupData],
      model: WelcomeModel,
      viewModel: Unit
  ): GlobalEvent => Outcome[Unit] =
    _ => Outcome(viewModel)

  def present(
      context: FrameContext[StartupData],
      model: WelcomeModel,
      viewModel: Unit
  ): Outcome[SceneUpdateFragment] =
    val size = context.startUpData.viewConfig.viewPort.size
    val center = context.startUpData.viewConfig.center
    Outcome {
      SceneUpdateFragment.empty
        .addLayer(
          Layer(
            Graphic(size, Material.Bitmap(Assets.spaceTitle)),
            Graphic(Size(640, 200), Material.Bitmap(Assets.gameTitle))
              .withRef(320, 100)
              .withPosition(center - Point(0, 200)),
            TextBox("Click to play!", 300, 100)
              .withFontFamily(FontFamily.sansSerif)
              .withColor(RGBA.White)
              .withFontSize(Pixels(32))
              .withPosition(center)
              .withRef(150, 0)
              .alignCenter
          )
        )
    }
    
}
