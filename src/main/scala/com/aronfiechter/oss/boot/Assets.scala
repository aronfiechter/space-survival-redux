package com.aronfiechter.oss.boot

import indigo._

object Assets {

  val gameTitle: AssetName = AssetName("gameTitle")
  val spaceTitle: AssetName     = AssetName("spaceTitle")
  object Spaceship {
    val ref: AssetName     = AssetName("n8-32")
    val jsonRef: AssetName = AssetName("n8-32 JSON")

    def assets: Set[AssetType] =
      Set(
        AssetType.Image(Spaceship.ref, AssetPath("assets/sprites/" + Spaceship.ref + ".png")),
        AssetType.Text(Spaceship.jsonRef, AssetPath("assets/sprites/" + Spaceship.ref + ".json"))
      )
  }

  object Space {
    val tilesRef: AssetName        = AssetName("space_tiles-64")
    val radiationVertShader: AssetName = AssetName("radiation-vert")
    val radiationFragShader: AssetName = AssetName("radiation-frag")
    
    def assets: Set[AssetType] =
      Set(
        AssetType.Image(tilesRef, AssetPath(s"assets/sprites/${tilesRef}.png")),
        AssetType.Text(radiationVertShader, AssetPath(s"assets/shaders/${radiationVertShader.toString}.frag")),
        AssetType.Text(radiationFragShader, AssetPath(s"assets/shaders/${radiationFragShader.toString}.frag")),
      )
  }

  object Portal {
    val portalBlendFragShader: AssetName = AssetName("portal-blend-frag")

    def assets: Set[AssetType] =
      Set(
        AssetType.Text(portalBlendFragShader, AssetPath(s"assets/shaders/${portalBlendFragShader.toString}.frag")),
      )
  }

  def assets: Set[AssetType] = Set(
    AssetType.Image(gameTitle, AssetPath("assets/logo.png")),
    AssetType.Image(spaceTitle, AssetPath("assets/sprites/space_small_720.png")),
  ) ++ Spaceship.assets
    ++ Space.assets
    ++ Portal.assets

  def fonts: Set[FontInfo] = Set()
}
