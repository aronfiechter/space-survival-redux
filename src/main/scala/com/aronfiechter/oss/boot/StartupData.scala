package com.aronfiechter.oss.boot

import com.aronfiechter.oss.constants.Constants.Space._
import com.aronfiechter.oss.model.core._
import indigo._
import indigo.json.Json
import indigo.syntax._

import scala.util.Failure
import scala.util.Random
import scala.util.Success
import scala.util.Try

final case class StartupData(
    viewConfig: ViewConfig,
    sprites: Sprites,
    clips: Clips,
    cloneBlanks: CloneBlanksHolder,
)

object StartupData {

  def setup(
      viewConfig: ViewConfig,
      assetCollection: AssetCollection,
      dice: Dice
  ): Outcome[Startup[StartupData]] =
    Outcome {
      val loader: (AssetName, AssetName, Depth) => Try[LoaderResult] =
        animationLoader(assetCollection, dice)

      val loadedData: Try[(Sprites, Clips, List[Animation])] = for {
        loadedSpaceship <- loader(Assets.Spaceship.jsonRef, Assets.Spaceship.ref, Depth.zero)
      } yield extractSpritesAndAnimations(loadedSpaceship)

      val loadedCloneBlanksHolder: Try[CloneBlanksHolder] = generateCloneBlanksHolder(dice)

      val setupResult = for {
        (sprites, clips, animations) <- loadedData
        clonkBlanksHolder <- loadedCloneBlanksHolder
      } yield (sprites, clips, animations, clonkBlanksHolder)

      setupResult match {
        case Success((sprites, clips, animations, clonkBlanksHolder)) =>
          Startup
            .Success(StartupData(viewConfig, sprites, clips, clonkBlanksHolder))
            .addAnimations(animations)
        case Failure(error) =>
          Startup.Failure(error.getMessage)
      }
    }

  private def extractSpritesAndAnimations(
      loadedSpaceship: LoaderResult
  ): (Sprites, Clips, List[Animation]) = {
    val sprites = Sprites(
      spaceship = loadedSpaceship.spriteAndAnimations.sprite
    )
    val clips = Clips(
      spaceship = loadedSpaceship.clips
    )
    val animations = List(
      loadedSpaceship.spriteAndAnimations.animations
    )
    (sprites, clips, animations)
  }

  private def animationLoader(
      assetCollection: AssetCollection,
      dice: Dice
  )(jsonRef: AssetName, name: AssetName, depth: Depth): Try[LoaderResult] = {
    val loaderResult = for {
      json                <- assetCollection.findTextDataByName(jsonRef)
      aseprite            <- Json.asepriteFromJson(json)
      spriteAndAnimations <- aseprite.toSpriteAndAnimations(dice, name)
      clips               <- aseprite.toClips(name)
    } yield {
      val finalSprite =
        spriteAndAnimations.copy(sprite = spriteAndAnimations.sprite.withDepth(depth))
      LoaderResult(finalSprite, clips)
    }

    loaderResult match {
      case Some(res) => Success(res)
      case None      => Failure(Error("Failed to load " + name))
    }
  }

  private def generateCloneBlanksHolder(
      dice: Dice
  ): Try[CloneBlanksHolder] = {
    val ref = Assets.Space.tilesRef
    val bitmap = Material.Bitmap(Assets.Space.tilesRef)
    val baseCrop = Rectangle(TILE_SIZE)
    val rawGraphics = (0 until TILE_VARIANTS)
      .map { x => baseCrop.moveBy(x * TILE_SIZE.width, 0) }
      .map { crop => Graphic(crop, bitmap)
        .moveTo(Point.zero)
        .withRef(32, 32)
        .moveBy(32, 32)
      }
      .toList
    val r = Random(dice.seed)
    val randomizedGraphics = r.shuffle(r.shuffle(rawGraphics))
    val spaceCloneBlanks = randomizedGraphics
      .zipWithIndex
      .map { case (graphic, i) =>
        val cloneId = CloneId(s"spaceTile-${i}")
        CloneBlank(cloneId, graphic)
      }.toBatch

    Success(CloneBlanksHolder(spaceCloneBlanks))
  }
}

final case class LoaderResult(
    val spriteAndAnimations: SpriteAndAnimations,
    val clips: ClipsMap
)

final case class Sprites(
    val spaceship: Sprite[Material.Bitmap]
)

final case class Clips(
    val spaceship: ClipsMap
)

final case class CloneBlanksHolder(
  val space: Batch[CloneBlank],
)
