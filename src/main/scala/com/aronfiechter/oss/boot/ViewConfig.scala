package com.aronfiechter.oss.boot

import indigo._

final case class ViewConfig() {
  val viewportWidth: Int  = 720
  val viewportHeight: Int = 720
  val zoomLevel: Int      = 1

  lazy val viewPort = GameViewport(viewportWidth, viewportHeight)

  def center = viewPort.giveDimensions(zoomLevel).center
  def topLeft = viewPort.giveDimensions(zoomLevel).topLeft
  def bottomLeft = viewPort.giveDimensions(zoomLevel).bottomLeft
  def bottomRight = viewPort.giveDimensions(zoomLevel).bottomRight
}

object ViewConfig {
  lazy val default = ViewConfig()
}
