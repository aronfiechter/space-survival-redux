package com.aronfiechter.oss.utils

import indigo._

extension (d: Vector2)
  def reflectionWithNormal(normal: Vector2): Vector2 =
    val n = normal.normalise
    d - n * 2 * (d.dot(n))

  def clampToMagnitude(magnitude: Double): Vector2 =
    if (d.length > magnitude) d.normalise * magnitude
    else d
