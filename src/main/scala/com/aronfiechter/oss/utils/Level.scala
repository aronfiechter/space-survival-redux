package com.aronfiechter.oss.utils

object Level {
  type LevelNumber = Int

  private val defaultSlowness = 50
  private def slowIncrease(slowness: Int)(
    initialValue: Int,
    maxValue: Int,
  ): LevelNumber => Int =
    level => ((maxValue - initialValue).toFloat * 
      (level.toFloat / (level.toFloat + 50.0)) + initialValue.toFloat).toInt

  private def defaultSlownessIncrease = slowIncrease(defaultSlowness)

  private def levelSize = defaultSlownessIncrease(40, 100)
  def size(levelNumber: LevelNumber): Int = levelSize(levelNumber)
}
