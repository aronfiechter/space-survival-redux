package com.aronfiechter.oss.utils

import indigo._

object Signals {
  def modulo(n: Int): Signal[Int] =
      Signal(t => t.toMillis.toInt % n).affectTime(0.01)
}
