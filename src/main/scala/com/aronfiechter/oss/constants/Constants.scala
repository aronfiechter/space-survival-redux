package com.aronfiechter.oss.constants

import indigo._

object Constants {

  object Space {
    val TILE_VARIANTS = 10
    val TILE_SIZE = Size(64)
  }

  object Portal {
    val MIN_DISTANCE = 360
  }

}
